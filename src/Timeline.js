import React, { Component } from 'react';
import './css/main.css';

class Timeline extends Component {
  render() {
    return (
      <div className="fl w-100 pa2">
        <div className="">
          <ul className="measure-wide">
            <li className="flex items-center lh-copy pa3 ph0-l bb b--black-10">
              <div className="pl1 flex-auto tl ">
                <i className="fa fa-bus  font-img red" aria-hidden="true" />
              </div>
              <div className="pl3 flex-auto">
                <span className="f6 db black-70">31/12/2018 - 00:01</span>
                <span className="f6 db black-90">Route Name</span>
              </div>
              <div>
                <span className="f6 link red">
                  <i className="fa fa-minus tr" aria-hidden="true" /> R$ 3,60
							</span>
              </div>
            </li>
            <li className="flex items-center lh-copy pa3 ph0-l bb b--black-10">
              <div className="pl1 flex-auto tl ">
                <i className="fa fa-bus  font-img red" aria-hidden="true" />
              </div>
              <div className="pl3 flex-auto">
                <span className="f6 db black-70">31/12/2018 - 00:01</span>
                <span className="f6 db black-90">Route Name</span>
              </div>
              <div>
                <span className="f6 link red">
                  <i className="fa fa-minus tr" aria-hidden="true" /> R$ 3,60
							</span>
              </div>
            </li>
            <li className="flex items-center lh-copy pa3 ph0-l bb b--black-10">
              <div className="pl1 flex-auto tl">
                <i className="fa fa-credit-card  font-img green" aria-hidden="true" />
              </div>
              <div className="pl3 flex-auto">
                <span className="f6 db black-70">31/12/2018 - 00:01</span>
                <span className="f6 db black-90">Recharge Card</span>
              </div>
              <div>
                <span className="f6 link green">
                  <i className="fa fa-plus tr" aria-hidden="true" /> R$ 50,00
							</span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Timeline;
