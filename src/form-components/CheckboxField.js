import React from 'react';
import './stylesheet.css';
import { replace } from 'ramda';

export default class CheckboxField extends React.Component {
  constructor(props) {
    super();
    this.state = {
      items: props.items,
      placeholder: props.placeholder,
      class: props.className + " title-box",
      msgErro: props.msgErro,
      errorOn: 'none',
      value: props.value,
      isError: props.isError,
      title: props.title
    };
    this.handleChange = this.handleChange.bind(this);
    this.validate = this.validate.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  validate() {
    this.setState((prevState) => ({
      isError: !prevState.isError
    }));
    if (this.state.isError) {
      this.setState({
        class: this.state.class + ' title-box-error'
      });
      this.setState({ errorOn: 'block' });
    } else {
      this.setState({
        class: replace(' title-box-error', '', this.state.class)
      });
      this.setState({ errorOn: 'none' });
    }
  }

  render() {
    let msgErrorVar = {
      display: this.state.errorOn
    };
    return (
      <div>
        <div className={this.state.class}>
          <div className="title">{this.state.title}</div>
          <div className="content">
            {this.state.items.map(function (item) {
              return (
                <label key={item.id} className={item.class}>
                  <input value={item.value} type="checkbox" id={item.id} /> {item.title}
                  <span className="checkmark" />
                </label>
              );
            })
            }
            <button onClick={this.validate}>Send Error</button>
          </div>
        </div>
        <span className="help-block" style={msgErrorVar}>
          {this.state.msgErro}
        </span>
      </div>
    );
  }
}
