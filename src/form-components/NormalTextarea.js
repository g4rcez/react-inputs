import React from 'react';
import './stylesheet.css';
import { replace } from 'ramda';

export default class NormalTextarea extends React.Component {
	constructor(props) {
		super();
		this.state = {
			id: props.id,
			placeholder: props.placeholder,
			class: props.className + ' normal-input',
			msgErro: props.msgErro,
			errorOn: 'none',
			value: props.value,
			isError: props.isError
		};
		this.handleChange = this.handleChange.bind(this);
		this.validate = this.validate.bind(this);
	}

	handleChange(event) {
		this.setState({ value: event.target.value });
	}

	validate() {
		this.setState((prevState) => ({ isError: !prevState.isError }));
		if (this.state.isError) {
			this.setState({ class: this.state.class + ' normal-input-error' });
			this.setState({ errorOn: 'block' });
		} else {
			this.setState({ class: replace('normal-input-error', '', this.state.class) });
			this.setState({ errorOn: 'none' });
		}
	}

	render() {
		let msgErrorVar = { display: this.state.errorOn };
		return (
			<div>
				<textarea
					placeholder={this.state.placeholder}
					className={this.state.class}
					id={this.state.id}
					onChange={this.handleChange}
					value={this.state.value}
				/>
				<span className="help-block" style={msgErrorVar}>
					{this.state.msgErro}
				</span>
				<button onClick={this.validate}>Send Error</button>
			</div>
		);
	}
}
