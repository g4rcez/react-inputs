import React from 'react';
import './stylesheet.css';
import { replace } from 'ramda';

export default class CheckboxField extends React.Component {
  constructor(props) {
    super();
    this.state = {
      items: props.items,
      placeholder: props.placeholder,
      class: props.className + " title-box",
      msgErro: props.msgErro,
      errorOn: 'none',
      value: props.value,
      isError: props.isError,
      title: props.title,
      selectedOption: props.items[0].value
    };
    this.handleChange = this.handleChange.bind(this);
    this.validate = this.validate.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  validate() {
    this.setState((prevState) => ({
      isError: !prevState.isError
    }));
    if (this.state.isError) {
      this.setState({
        style: this.state.style + ' title-box-error'
      });
      this.setState({ errorOn: 'block' });
    } else {
      this.setState({
        style: replace(' title-box-error', '', this.state.style)
      });
      this.setState({ errorOn: 'none' });
    }
  }

  handleOptionChange(changeEvent) {
    this.setState({
      selectedOption: changeEvent.target.value
    });
  }

  render() {
    let msgErrorVar = {
      display: this.state.errorOn
    };
    return (
      <div>
        <div className={this.state.class}>
          <div className="title">{this.state.title}</div>
          <div className="content">
            {this.state.items.map(function (item) {
              return (
                <label key={item.id} className={item.style}>
                  <input className={item.class + " radio-box"} onChange={this.handleOptionChange} value={item.value} type="radio" id={item.id} checked={this.state.selectedOption === item.value} /> {item.title}
                  <span className="checkmark" />
                </label>
              );
            }, this)
            }
            <button onClick={this.validate}>Send Error</button>
          </div>
        </div>
        <span className="help-block" style={msgErrorVar}>
          {this.state.msgErro}
        </span>
      </div>
    );
  }
}
