import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import NormalInput from './form-components/NormalInput';
import NormalTextarea from './form-components/NormalTextarea';
import CheckboxField from "./form-components/CheckboxField";
import RadioboxField from "./form-components/RadioboxField";

ReactDOM.render(
    [
        <div>
            <div className="pa3">
                <NormalInput
                    id="email"
                    placeholder="Campo de Email foobar@foo.bar"
                    mask="email"
                    guide="false"
                    isError="false"
                    msgErro="Email não formatado"
                />
            </div>
            <div className="pa3">
                <NormalInput
                    id="cellphone"
                    placeholder="Campo de celular: 21 999 888 777..."
                    mask="cellphone"
                    vaisErrorlid="false"
                    msgErro="Celular no formato inválido"
                />
            </div>
            <div className="pa3">
                <NormalInput
                    id="name"
                    placeholder="Fulano de Ciclano Beltrano Arano..."
                    mask="name"
                    isError="false"
                    msgErro="\_('-')_/"
                />
            </div>
            <div className="pa3">
                <NormalInput
                    id="date"
                    placeholder="Campo de data __/__/____"
                    mask="date"
                    isError="false"
                    msgErro="Data inválida"
                />
            </div>
            <div className="pa3">
                <NormalInput
                    id="cpf"
                    placeholder="Campo de CPF 123.456.789-10"
                    mask="cpf"
                    isError="false"
                    msgErro="CPF Inválido"
                />
            </div>
            <div className="pa3">
                <NormalTextarea
                    id="texto"
                    placeholder="Redação do Enem"
                    isError="false"
                    msgErro="Texto inválido"
                />
            </div>
            <div className="pa3">
                <CheckboxField
                    items={[{
                        id: "ola",
                        title: "Não quero",
                        value: "Não1",
                        class: "checkbox-container",
                    }, {
                        id: "ola2",
                        title: "Não quero mesmo",
                        value: "Não2",
                        class: "checkbox-container"
                    }, {
                        id: "ola3",
                        title: "Tenho certeza que não quero",
                        value: "Não3",
                        class: "checkbox-container"
                    }]}
                    title="Escolha várias opções abaixo"
                    isError="false"
                    msgErro="Selecione uma das opções"
                />
            </div>
            <div className="pa3">
                <RadioboxField
                    items={[{
                        id: "ola",
                        title: "Não quero",
                        value: "Não1",
                        class: "checkbox-container",
                    }, {
                        id: "ola2",
                        title: "Não quero mesmo",
                        value: "Não2",
                        class: "checkbox-container"
                    }, {
                        id: "ola3",
                        title: "Tenho certeza que não quero",
                        value: "Não3",
                        class: "checkbox-container"
                    }]}
                    title="Escolha várias opções abaixo"
                    isError="false"
                    msgErro="Selecione uma das opções"
                />
            </div>
        </div>
    ],
    document.getElementById('root')
)
    ;
registerServiceWorker();
